package facci.alexsantacruz.practica01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText editTextFahrenheit;
    private EditText editTextCentigrados;
    private TextView textViewResultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e("error","Alex David Santacruz Morales");

        editTextFahrenheit = findViewById(R.id.editTextFahrenheit);
        editTextCentigrados = findViewById(R.id.editTextCentigrados);
        textViewResultado = findViewById(R.id.textViewResultado);
    }


    public void convertir(View view ){
        if (!editTextFahrenheit.getText().toString().isEmpty() && !editTextCentigrados.getText().toString().isEmpty()){
            textViewResultado.setText(R.string.adver1);
        }else {
            textViewResultado.setText(R.string.adver2);
        }

        if (!editTextFahrenheit.getText().toString().isEmpty() && editTextCentigrados.getText().toString().isEmpty()){
            double operacion = (Double.parseDouble(editTextFahrenheit.getText().toString()) -32)/1.8;
            textViewResultado.setText(operacion+" C");
        }

        if (!editTextCentigrados.getText().toString().isEmpty() && editTextFahrenheit.getText().toString().isEmpty()){
            double operacion = (Double.parseDouble(editTextCentigrados.getText().toString()) * 9/5) +32;
            textViewResultado.setText(operacion+" F");
        }
    }

}